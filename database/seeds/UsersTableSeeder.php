<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//admin
    	DB::table('users')->insert([
        	'name'=>'Adam Smith',
        	'email'=>'adam@smith.com',
        	'password'=>bcrypt('password'),
        	'phone'=>12345678,
        	'address'=>"14 Tottenham Court Road, London, England, W1T 1JY",
        	'status'=>'Active',
        	'pesel'=>'5641214',
        	'type'=>'admin'

        	]);

    	//patient 1
        DB::table('users')->insert([
        	'name'=>'John Snow',
        	'email'=>'john@snow.com',
        	'password'=>bcrypt('password'),
        	'phone'=>325235352,
        	'address'=>"61 Wellfield Road, Roath, Cardiff, CF24 3DG",
        	'status'=>'Active',
        	'pesel'=>'4701232133',
        	'type'=>'patient'

        	]);

    	//patient 2
        DB::table('users')->insert([
        	'name'=>'Anna Jackson',
        	'email'=>'anna@jackson.com',
        	'password'=>bcrypt('password'),
        	'phone'=>198765213,
        	'address'=>"44-46 Morningside Road, Edinburgh, Scotland, EH10 4BF",
        	'status'=>'Active',
        	'pesel'=>'781231233',
        	'type'=>'patient'

        	]);



    	//doctor 1
        DB::table('users')->insert([
        	'name'=>'Steve Phillips',
        	'email'=>'steve@phillips.com',
        	'password'=>bcrypt('password'),
        	'phone'=>88879123003,
        	'address'=>"27 Colmore Row, Birmingham, England, B3 2EW",
        	'status'=>'Active',
        	'pesel'=>'798798712',
        	'type'=>'doctor'

        	]);

    	//doctor 2
        DB::table('users')->insert([
        	'name'=>'Monica Johnson',
        	'email'=>'monica@johnson.com',
        	'password'=>bcrypt('password'),
        	'phone'=>34234234,
        	'address'=>"91 Western Road, Brighton, East Sussex, England, BN1 2NW",
        	'status'=>'Active',
        	'pesel'=>'781231233',
        	'type'=>'doctor'

        	]);

        //specialization 1
        DB::table('specializations')->insert([
        	'name'=>'oncology'
			]);

        //specialization 2
        DB::table('specializations')->insert([
        	'name'=>'surgeon'
			]);

        //specialization 3
        DB::table('specializations')->insert([
        	'name'=>'internist'
			]);
    }
}
