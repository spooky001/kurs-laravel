@extends('template')
 
@section('title')
  @if (isset($title))
    - {{ $title }} 
  @endif
@endsection

@section('content')
<div class="container">
<h2>Wizyty</h2>
<a href="{{ URL::to('visits/create') }}">Dodaj nową wizytę</a>
 <table class="table">
      <thead>
        <tr>
          <th>#</th>
          <th>Pacjent</th>
          <th>Lekarz</th>
          <th>Data</th>
        </tr>
      </thead>
      <tbody>
      @foreach ($visits as $visit)
        <tr>
          <th scope="row">{{ $visit->id }}</th>
          <td>{{ $visit->patient->name}} ({{ $visit->patient->pesel}})</td>
          <td>{{ $visit->doctor->name }}</td>
          <td>{{ $visit->date }}</td>
          </tr>
 
       @endforeach
      </tbody>
    </table>
</div>
@endsection('content')