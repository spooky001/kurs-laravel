@extends('template')
 
@section('title')
  @if (isset($title))
    - {{ $title }} 
  @endif
@endsection

@section('content')
<div class="container">
<h2>Pacjenci</h2>
 <table class="table">
      <thead>
        <tr>
          <th>#</th>
          <th>Nazwisko</th>
          <th>E-mail</th>
          <th>Telefon</th>
        </tr>
      </thead>
      <tbody>
      @foreach ($patientsList as $patient)
        <tr>
          <th scope="row">{{ $patient->id }}</th>
          <td><a href="{{ URL::to('patients/' . $patient->id ) }}"> {{ $patient->name }}</a></td>
          <td>{{ $patient->email }}</td>
          <td>{{ $patient->phone }}</td>
        </tr>
 
       @endforeach
      </tbody>
    </table>

    
    
</div>
@endsection('content')

  