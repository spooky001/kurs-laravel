<style>
  .g-recaptcha, .btn{
    margin: 0 auto;
    display: table;

  }
  .captcha-wrapper{
    margin:0 auto;
    text-align: center;
  }
  .captcha-table{
    width:100%;
  }

</style>

<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script
      src="https://code.jquery.com/jquery-1.12.4.js"
      integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU="
      crossorigin="anonymous"></script>
      <!-- Plotly.js -->
      <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
      <script src='https://www.google.com/recaptcha/api.js'></script>


      <?php

      $pytania = array(
            "Pytanie A",
            "Pytanie B",
            "Pytanie C",
            "Pytanie D",
      );

      $odpowiedzi = array(
          "Odpowiedz A",
          "Odpowiedz B",
          "Odpowiedz C",
          "Odpowiedz D",
          "Odpowiedz E",
      );

      $odpowiedziWzorcowe_A = array(
          5,
          4,
          2,
          1,
          3,
      );
      $odpowiedziWzorcowe_B = array(
          1,
          3,
          4,
          1,
          2,
      );
      $odpowiedziWzorcowe_C = array(
          5,
          5,
          4,
          3,
          2,
      );
      $odpowiedziWzorcowe_D = array(
          5,
          5,
          4,
          3,
          2,
      );

      ?>


      <script>
      $(document).ready(function(){
        $('.walk_to_the_moon').submit(function(e){
          var questions = <?php echo count($pytania); ?>;
          for(i=0;i<questions;i++) {
            if (!$("input[name='pytanie_"+i +"']:checked").val()) {
                alert('Uzupełnij wszystkie odpowiedzi!');
                e.preventDefault();
                 return false;
             }
          }


        });

      });



      </script>


      <?php
      if ($_SERVER['REQUEST_METHOD'] === 'POST') {


          $recaptcha_secret = "6Lf4LJUUAAAAAFdNM6AlkJXwK-RjkD0MOc4ZMkQL";
          $response = file_get_contents(
            "https://www.google.com/recaptcha/api/siteverify?secret=".$recaptcha_secret."&response=".$_POST['g-recaptcha-response']);
            $response = json_decode($response, true);

            if($response["success"] === true)
            {
                $values = array();
                foreach($pytania as $key=>$p) {
                  $values[] = $_POST['pytanie_'.$key];

                }
                ?>

                <div id="myDiv"></div>
                <script type="text/javascript">
                var trace1 = {
                  x: <?php echo json_encode($pytania, JSON_NUMERIC_CHECK); ?>,
                  y:  <?php echo json_encode($values, JSON_NUMERIC_CHECK); ?>,
                  name: "Moje odpowiedzi",
                  type: 'bar'
                };


                      var trace2 = {
                        x: <?php echo json_encode($pytania, JSON_NUMERIC_CHECK); ?>,
                        y:  <?php echo json_encode($odpowiedziWzorcowe_A, JSON_NUMERIC_CHECK); ?>,
                        name: "<?php echo $pytania[0]; ?>",
                        type: 'bar'
                      };

                      var trace3 = {
                        x: <?php echo json_encode($pytania, JSON_NUMERIC_CHECK); ?>,
                        y:  <?php echo json_encode($odpowiedziWzorcowe_B, JSON_NUMERIC_CHECK); ?>,
                        name: "<?php echo $pytania[1]; ?>",
                        type: 'bar'
                      };

                      var trace4 = {
                        x: <?php echo json_encode($pytania, JSON_NUMERIC_CHECK); ?>,
                        y:  <?php echo json_encode($odpowiedziWzorcowe_C, JSON_NUMERIC_CHECK); ?>,
                        name: "<?php echo $pytania[2]; ?>",
                        type: 'bar'
                      };

                      var trace5 = {
                        x: <?php echo json_encode($pytania, JSON_NUMERIC_CHECK); ?>,
                        y:  <?php echo json_encode($odpowiedziWzorcowe_D, JSON_NUMERIC_CHECK); ?>,
                        name: "<?php echo $pytania[3]; ?>",
                        type: 'bar'
                      };

                var data = [trace1, trace2, trace3, trace4];

                var layout = {barmode: 'group'};

                Plotly.newPlot('myDiv', data, layout);
                </script>
                <?php
            }

    } else {
      if(isset($_GET['action']) && $_GET['action'] == 'results'){

        ?>
        <div id="myDiv-results"></div>
        <script type="text/javascript">
              var trace2 = {
                x: <?php echo json_encode($pytania, JSON_NUMERIC_CHECK); ?>,
                y:  <?php echo json_encode($odpowiedziWzorcowe_A, JSON_NUMERIC_CHECK); ?>,
                name: "<?php echo $pytania[0]; ?>",
                type: 'bar'
              };

              var trace3 = {
                x: <?php echo json_encode($pytania, JSON_NUMERIC_CHECK); ?>,
                y:  <?php echo json_encode($odpowiedziWzorcowe_B, JSON_NUMERIC_CHECK); ?>,
                name: "<?php echo $pytania[1]; ?>",
                type: 'bar'
              };

              var trace4 = {
                x: <?php echo json_encode($pytania, JSON_NUMERIC_CHECK); ?>,
                y:  <?php echo json_encode($odpowiedziWzorcowe_C, JSON_NUMERIC_CHECK); ?>,
                name: "<?php echo $pytania[2]; ?>",
                type: 'bar'
              };

              var trace5 = {
                x: <?php echo json_encode($pytania, JSON_NUMERIC_CHECK); ?>,
                y:  <?php echo json_encode($odpowiedziWzorcowe_D, JSON_NUMERIC_CHECK); ?>,
                name: "<?php echo $pytania[3]; ?>",
                type: 'bar'
              };

        var data = [trace2, trace3, trace4];

        var layout = {barmode: 'group'};

        Plotly.newPlot('myDiv-results', data, layout);
        </script>




        <?php
            } else {
      echo "<form method='POST' action='' class='walk_to_the_moon'>";
        echo "<table class='table'>";
          echo "<tr>";
            echo "<td></td>";
              foreach($odpowiedzi as $odp) {
                echo "<td>" . $odp .  "</td>";
              }
          echo "</tr>";

          foreach($pytania as $key=>$p) {
            echo "<tr>";
            echo "<td>" . $p .  "</td>";
            $i = 0;
            foreach($odpowiedzi as $value) {
              $i++;
              echo "<td><input type='radio' name='pytanie_". $key . "' value='" . $i . "'></td>";
            }


            echo "</tr>";
          }
        echo "</table>";

        echo "<table class='captcha-table'>";
          echo "<tr>";
            echo "<td class=\"captcha-wrapper\"> <div class=\"g-recaptcha\" data-sitekey=\"6Lf4LJUUAAAAALb5Qq65KNFYE2n6vfYYLXE2Z-nP\"></div></td>";
          echo "</tr>";
            echo "<tr>";
                echo "<td class=\"captcha-wrapper\"> <input class=\"btn btn-primary\" type='submit' value='Dodaj'><br/>  <a href=\"?action=results\">Pokaż wyniki</a></td>";
            echo "</tr>";
        echo "</table>";
      echo "</form>";



      }
}
