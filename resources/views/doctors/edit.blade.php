@extends('template')
 
@section('title')
  @if (isset($title))
    - {{ $title }} 
  @endif
@endsection

@section('content')
<div class="container">
<h2>Edycja lekarza</h2>
  <form action="{{ action('DoctorController@store')}}" method="POST" role="form">
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <div class="form-group">
      <label for="name">Nazwisko i imię</label>
      <input type="text" class="form-control" name="name" value="{{ $doctor->name }}" />
    </div>

    <div class="form-group">
      <label for="email">Email</label>
      <input type="text" class="form-control" name="email" value="{{ $doctor->email }}"/>
    </div>

    <div class="form-group">
      <label for="phone">Telefon</label>
      <input type="phone" class="form-control" name="phone"  value="{{ $doctor->phone }}"/>
    </div>


    <div class="form-group">
      <label for="address">Adres</label>
      <input type="address" class="form-control" name="address"  value="{{ $doctor->address }}"/>
    </div>

    <div class="form-group">
      <label for="pesel">PESEL</label>
      <input type="pesel" class="form-control" name="pesel"  value="{{ $doctor->pesel }}"/>
    </div>


    <div class="form-group">
      <label for="pesel">Specjalizacja</label>
      @foreach($specializations as $specialization)

        <input type="checkbox" class="form-control" name="specializations[]" value="{{ $specialization->id }} "/>{{ $specialization->name }}
      @endforeach
    </div>


    <input type="hidden" name="status" value="Active" />

    <input type="submit" value="Dodaj" class="btn btn-primary"/>
  </form>
</div>
@endsection('content')