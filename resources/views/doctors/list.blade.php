@extends('template')

@section('title')
  @if (isset($title))
    - {{ $title }}
  @endif
@endsection

@section('content')
<div class="container">
<h2>Lekarze</h2>

<a href="{{ URL::to('doctors/create' ) }}"> Dodaj lekarza</a>
 <table class="table">
      <thead>
        <tr>
          <th>#</th>
          <th>Nazwisko</th>
          <th>E-mail</th>
          <th>Telefon</th>
          <th>Specjalizacja</th>
          <th>Status</th>
          <th>Operacje</th>
        </tr>
      </thead>
      <tbody>
      @foreach ($doctorsList as $doctor)


        <tr>
          <th scope="row">{{ $doctor->id }}</th>
          <td><a href="{{ URL::to('doctors/' . $doctor->id ) }}"> {{ $doctor->name }}</a></td>
          <td>{{ $doctor->email }}</td>
          <td>{{ $doctor->phone }}</td>
          <td>
              <ul>
              @foreach ($doctor->specializations as $specialization)

                <li>{{ $specialization->name }}</li>
              @endforeach
              </ul>
          </td>
          <td>
            @foreach ($doctor->doctorsVisits as $visit)
            {{$visit->patient->name}}

            @endforeach</td>
          <td><a href="{{ URL::to('doctors/delete/' . $doctor->id ) }}" onclick="return confirm('Czy na pewno usunąć?')"> Usuń lekarza</a><br/>
          <a href="{{ URL::to('doctors/edit/' . $doctor->id ) }}" > Edycja lekarza</a></td>
        </tr>

       @endforeach
      </tbody>
    </table>


      @foreach ($statistics as $stat)
        @if ($stat->status == "Active")
            Liczba lekarzy dostępnych: {{ $stat->user_count }}<br/>
        @endif
        @if ($stat->status == "Inactive")
            Liczba lekarzy niedostępnych: {{ $stat->user_count }}
        @endif
      @endforeach

</div>
@endsection('content')
